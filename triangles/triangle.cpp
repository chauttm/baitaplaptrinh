#include <iostream>

using namespace std;

void leftTriangle(int size) 
{	
	for (int row = 1; row <= size; row ++) {
		int length = row;
		for (int col = 0; col < length; col++) {
			cout << "* ";
		}
		cout << endl;
	}
}

void rightTriangle(int size) 
{
	for (int row = 1; row <= size; row ++) {
		int length = row; 
		int paddings = size - length;
		
		for (int i = 0; i < paddings; i++) {
			cout << "  ";
		}
		
		for (int col = 0; col < length; col++) {
			cout << "* ";
		}
		cout << endl;
	}
}

void upSideDown(int size) 
{
	for (int row = 0; row <= size; row ++) {
		int length = (size-row) * 2 + 1; 
		int paddings = row;
		
		for (int i = 0; i < paddings; i++) {
			cout << "  ";
		}
		
		for (int col = 0; col < length; col++) {
			cout << "* ";
		}
		cout << endl;
	}
}

int main() {
	leftTriangle(6);
	rightTriangle(6);
	upSideDown(6);
	return 0;
}