#include <stdio.h>

void leftTriangle(int size) {
	int row, col, length;
	
	for (row = 1; row <= size; row ++) {
		length = row;
		for (col = 0; col < length; col++) {
			printf("* ");
		}
		printf("\n");
	}
}

void rightTriangle(int size) {
	int row, col, length, paddings, i;
	
	for (row = 1; row <= size; row ++) {
		length = row; 
		paddings = size - length;
		
		for (i = 0; i < paddings; i++) {
			printf("  ");
		}
		
		for (col = 0; col < length; col++) {
			printf("* ");
		}
		printf("\n");
	}
}

void upSideDown(int size) {
	int row, col, length, paddings, i;
	
	for (row = 0; row <= size; row ++) {
		length = (size-row) * 2 + 1; 
		paddings = row;
		
		for (i = 0; i < paddings; i++) {
			printf("  ");
		}
		
		for (col = 0; col < length; col++) {
			printf("* ");
		}
		printf("\n");
	}
}

int main() {
	leftTriangle(6);
	rightTriangle(6);
	upSideDown(6);
	return 0;
}